function counterFactory() {
    let count = 30
    return {
        increment() {
            count = count + 1
            return count;
        },
        decrement() {
            count = count - 1
            return count;
        }
    }
}
module.exports = counterFactory()
