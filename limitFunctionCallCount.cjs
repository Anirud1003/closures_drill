function limitFunctionCallCount(callBack, num) {
    limit = 0;
    
    return function limitFunction() {
        if (limit < num) {
            limit =limit + 1;
            return callBack();
        }else {
            return null;
        }
    }
}
module.exports = limitFunctionCallCount;

